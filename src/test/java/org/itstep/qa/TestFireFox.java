package org.itstep.qa;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestFireFox {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver32.exe");
        WebDriver driver = new FirefoxDriver();
        driver.navigate().to ("https://www.onliner.by/");
        driver.manage().window().maximize();
    }
}
