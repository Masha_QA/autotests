package org.itstep.qa;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class TestChrome {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to ("https://www.onliner.by/");
        driver.manage().window().maximize();
    }
}
